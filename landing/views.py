from django.shortcuts import render, redirect
from .models import Status

# Create your views here.


def index(request):
    status_list = Status.objects.all()
    return render(request, 'index.html', {'status': status_list})


def submit(request):
    if request.method == 'POST':
        stat = request.POST.get('input')
        if (0 < len(stat) <= 300):
            new_status = Status(status=stat)
            new_status.save()
        return redirect('landing:home')
