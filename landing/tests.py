from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.apps import apps

from .apps import LandingConfig
from .views import index, submit
from .models import Status

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.


class UnitTest(TestCase):
    def test_landing_app(self):
        self.assertEqual(LandingConfig.name, 'landing')
        self.assertEqual(apps.get_app_config(
            'landing').name, 'landing')

    def test_landing_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 404)

    def test_landing_using_index_func(self):
        found = reverse('landing:home')
        self.assertEqual(resolve(found).func, index)

    def test_submit_using_submit_func(self):
        found = reverse('landing:submit')
        self.assertEqual(resolve(found).func, submit)

    def test_landing_using_correct_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
        self.assertContains(response, 'Halo, apa Kabar!')

    def test_new_status_object(self):
        status = Status.objects.create(status='example')
        self.assertTrue(isinstance(status, Status))
        self.assertEqual(status.status, 'example')

    def test_status_shown(self):
        status = Status.objects.create(status='example')
        status.save()
        response = Client().get('/')
        self.assertContains(response, 'example')

    # def test_status_post(self):
    #     response = Client().post(reverse('landing:submit'), {
    #         'status': 'example'
    #     })

    #     self.assertEqual(response.status_code, 302)


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        # For local test
        # self.selenium = webdriver.Chrome('chromedriver.exe', chrome_options=chrome_options)
        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        self.selenium.get(self.live_server_url)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_post(self):
        selenium = self.selenium
        # find the form element
        content = selenium.find_element_by_id('input')
        submit = selenium.find_element_by_id('submit')
        # Fill the form with data
        content.send_keys('Tes Konten')
        # submitting the form
        submit.send_keys(Keys.RETURN)

        self.assertIn('Tes Konten', selenium.page_source)
