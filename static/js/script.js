$(document).ready(() => {
    $('ul').click(() => {
        $('ul').toggleClass('active')
        $('section').toggleClass('dark')
    });
    $(function () {
        $('#accordion').accordion({
            collapsible: true,
            active: false
        });
    });
});
