$(document).ready(() => {
    $('ul').click(() => {
        $('ul').toggleClass('active')
        $('body').toggleClass('dark')
    });
    $(function () {
        $('#accordion').accordion({
            collapsible: true,
            active: false
        });
    });
});
