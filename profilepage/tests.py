from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.apps import apps

from .apps import ProfilepageConfig
from .views import index


class UnitTest(TestCase):
    def test_profile_app(self):
        self.assertEqual(ProfilepageConfig.name, 'profilepage')
        self.assertEqual(apps.get_app_config(
            'profilepage').name, 'profilepage')

    def test_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 404)

    def test_profile_using_index_func(self):
        found = reverse('profile:profile')
        self.assertEqual(resolve(found).func, index)

    def test_profile_using_correct_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile/index.html')
        self.assertContains(response, 'Halo, Nama gw Huda!')
